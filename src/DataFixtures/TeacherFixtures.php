<?php

namespace App\DataFixtures;

use App\Entity\Teacher;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class TeacherFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');
        for($i = 1; $i <= 8; $i++) {
            $teacher = new Teacher();
            $teacher->setFirstName($faker->firstName);
            $teacher->setLastName($faker->lastName);
            $teacher->setEmail($teacher->getFirstName().'.'.$teacher->getLastName().'@gmail.com');
            $teacher->setImage($teacher->getFirstName().$teacher->getLastName().'.jpg');
            $manager->persist($teacher);
        }

        $manager->flush();
    }
}
