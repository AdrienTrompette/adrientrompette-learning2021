<?php

namespace App\Form;

use App\Entity\Course;
use App\Entity\Teacher;
use Doctrine\ORM\EntityRepository;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class EditCourseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom'
            ])
            ->add('smallDescription', TextType::class, [
                'label' => 'Description courte'
            ])
            ->add('fullDescription', CKEditorType::class, [
                'label' => 'Description complète',
            ])
            ->add('duration', IntegerType::class, [
                'label' => 'Durée de la formation'
            ])
            ->add('price', MoneyType::class, [
                'label' => 'Prix de la formation'
            ])
            ->add('imageFile', VichImageType::class, [
                'label' => 'Image de la formation',
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false
            ])
            ->add('schedule', TextType::class, [
                'label' => 'Horaire de la formation'
            ])
            ->add('programFile', VichFileType::class, [
                'label' => 'Programme de la formation',
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false
            ])
            ->add('category', EntityType::class, [
                'label' => 'Catégorie',
                'placeholder' => 'Sélectionnez...',
                'class' => 'App:CourseCategory',
                'choice_label' => 'name',
                'query_builder' => function(EntityRepository $repository) {
                    return $repository->createQueryBuilder('c')->orderBy('c.name', 'ASC');
                }
            ])
            ->add('level', EntityType::class, [
                'label' => 'Niveau de la formation',
                'placeholder' => 'Sélectionnez...',
                'class' => 'App:CourseLevel',
                'choice_label' => 'name'
            ])
            ->add('teacher', EntityType::class, [
                'label' => 'Professeur',
                'placeholder' => 'Sélectionnez',
                'class' => 'App:Teacher',
                'choice_label' => 'lastName'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Course::class,
        ]);
    }
}
