<?php

namespace App\Controller;

use App\Entity\Course;
use App\Repository\CourseRepository;
use App\Repository\TeacherRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PagesController extends AbstractController
{
    /**
     * @return Response
     */
    #[Route('/about', name: 'about')]
    public function about(): Response
    {
        return $this->render('pages/about.html.twig');
    }


    /**
     * @param UserRepository $userRepository
     * @return Response
     */
    #[Route('/team', name: 'team')]
    public function team(UserRepository $userRepository): Response
    {
        $team = $userRepository->findBy(
            [],
            ['roles' => 'ASC']
        );

        return $this->render('pages/team.html.twig', [
            'team' => $team
        ]);
    }


    /**
     * @param TeacherRepository $teacherRepository
     * @return Response
     */
    #[Route('/teachers', name:'teachers')]
    public function teacher(TeacherRepository $teacherRepository): Response
    {
        $teachers = $teacherRepository->findBy(
            [],
            ['lastName' => 'ASC']
        );

        return $this->render('pages/teachers.html.twig', [
            'teachers' => $teachers,
        ]);

    }
}
