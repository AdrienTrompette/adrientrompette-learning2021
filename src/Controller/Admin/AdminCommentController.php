<?php

namespace App\Controller\Admin;

use App\Entity\Comment;
use App\Entity\User;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminCommentController extends AbstractController
{
    /**
     * @param CommentRepository $commentRepository
     * @return Response
     */
    #[Route('/admin/comments', name: 'admin_comments')]
    public function adminComments(CommentRepository $commentRepository): Response
    {
        $comments = $commentRepository->findBy(
            [],
            ['createdAt' => 'ASC']
        );

        return $this->render('admin/comments/comments.html.twig', [
            'comments' => $comments,
        ]);
    }

    /**
     * @param Comment $comment
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/admin/delcomments/{id}', name: 'admin_del_comment')]
    public function delComment(Comment $comment, EntityManagerInterface $manager): Response
    {
        $manager->remove($comment);
        $manager->flush();
        $this->addFlash(
            'success',
            'Le commentaire '.$comment->getTitle().' a bien été supprimé'
        );
        return $this->redirectToRoute('admin_comments');
    }

    /**
     * @param Comment $comment
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/admin/viewcomment/{id}', name: 'admin_view_comment')]
    public function viewComment(Comment $comment, EntityManagerInterface $manager): Response
    {
        $comment->setIsPublished(!$comment->getIsPublished());
        $manager->flush();

        return $this->redirectToRoute('admin_comments');
    }
}
