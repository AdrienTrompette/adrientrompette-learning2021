<?php

namespace App\Controller\Admin;

use App\Entity\Course;
use App\Entity\User;
use App\Form\UserAdminType;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class AdminUserController extends AbstractController
{
    /**
     * @param UserRepository $repository
     * @return Response
     */
    #[Route('/admin/users', name: 'admin_users')]
    public function adminUsers(UserRepository $repository): Response
    {
        $users = $repository->findBy(
            [],
            ['createdAt'    => 'DESC']
        );

        return $this->render('admin/users/users.html.twig', [
            'users' => $users
        ]);
    }

    private $hasher;

    public function __construct(UserPasswordHasherInterface $hasher) {
        $this->hasher = $hasher;
    }

    #[Route('/admin/newuser', name: 'admin_new_user')]
    public function newUser(EntityManagerInterface $manager,UserPasswordHasherInterface $userPasswordHasherInterface, Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $user->setRoles(['ROLE_USER']);
            $user->setPassword(
                $userPasswordHasherInterface->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            if(empty($user->getImageFile()) ? $user->setImage('default.jpg') : $user->getImageFile());
            $user->setCreatedAt(new \DateTimeImmutable());
            $user->setUpdatedAt(new \DateTimeImmutable());
            $user->setLastLogAt(new \DateTimeImmutable());
            $user->setIsDisabled(false);
            $manager->persist($user);
            $manager->flush();
            $this->addFlash(
                'success',
                'L\'utilisateur '.$user->getFirstName().' '.$user->getLastName().' a bien été ajouté'
            );
            $this->redirectToRoute('admin_users');
        }
        return $this->renderForm('admin/users/newuser.html.twig', [
            'form' => $form
        ]);
    }

    /**
     * @param User $user
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('admin/viewuser/{id}', name: 'admin_view_user')]
    public function viewUsers(User $user, EntityManagerInterface $manager): Response
    {
        $user->setIsDisabled(!$user->getIsDisabled());
        $manager->flush();

        return $this->redirectToRoute('admin_users');
    }

    #[Route('admin/edituser/{id}', name:'admin_edit_user')]
    public function editUser(User $user, EntityManagerInterface $manager, Request $request)
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($user);
            $manager->flush();
            $this->addFlash(
                'success',
                'L\'utilisateur a bien été modifié'
            );
            return $this->redirectToRoute('admin_users');
        }
        return $this->renderForm('admin/users/edituser.html.twig', [
            'form' => $form
        ]);
    }

    /**
     * @param User $user
     * @param EntityManagerInterface $manager
     * @param $role
     * @return RedirectResponse
     */
    #[Route('admin/user-promote/{id}/{role}', name: 'admin_user_promote')]
    public function promote(User $user, EntityManagerInterface $manager, $role)
    {
        $user->setRoles([$role]);
        $manager->flush();
        return $this->redirectToRoute('admin_users');
    }
}