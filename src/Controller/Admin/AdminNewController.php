<?php

namespace App\Controller\Admin;

use App\Entity\News;
use App\Form\EditNewType;
use App\Repository\NewsRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminNewController extends AbstractController
{
    #[Route('/admin/news', name: 'admin_news')]
    public function adminNews(NewsRepository $newsRepository): Response
    {
        $news = $newsRepository->findBy(
            [],
            ['createdAt' => 'ASC']
        );

        return $this->render('admin/news/news.html.twig', [
            'news' => $news,
        ]);
    }

    /**
     * @param News $news
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @return Response
     */
    #[Route('admin/editnew/{id}', name:'admin_edit_new')]
    public function editNew(News $news, EntityManagerInterface $manager, Request $request): Response
    {
        $form = $this->createForm(EditNewType::class, $news);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $slug = new Slugify();
            $news->setSlug($slug->slugify($news->getName()));
            $news->setCreatedAt(new \DateTimeImmutable());
            $news->setUpdatedAt(new \DateTimeImmutable());
            $news->setIsPublished(true);
            $manager->persist($news);
            $manager->flush();
            $this->addFlash(
                'success',
                'La new '.$news->getName().' a bien été modifiée'
            );
            unset($news);
            return $this->redirectToRoute('admin_news');
        }
        return $this->renderForm('admin/news/editnew.html.twig', [
            'form' => $form
        ]);
    }

    /**
     * @param News $news
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/admin/viewnew/{id}', name: 'admin_view_new')]
    public function viewNew(News $news, EntityManagerInterface $manager): Response
    {
        $news->setIsPublished(!$news->getIsPublished());
        $manager->flush();
        return $this->redirectToRoute('admin_news');
    }


    /**
     * @param News $new
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/admin/delnew/{id}', name: 'admin_del_new')]
    public function delNew(News $new, EntityManagerInterface $manager): Response
    {
        $manager->remove($new);
        $manager->flush();
        $this->addFlash(
            'success',
            'La new '.$new->getName().' a bien été supprimée'
        );
        return $this->redirectToRoute('admin_news');
    }
}
