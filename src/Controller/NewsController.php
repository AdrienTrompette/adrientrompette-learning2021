<?php

namespace App\Controller;

use App\Entity\News;
use App\Form\EditNewType;
use App\Form\NewType;
use App\Repository\NewsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NewsController extends AbstractController
{
    #[Route('/news', name: 'news')]
    public function news(NewsRepository $repository, PaginatorInterface $paginator, Request $request): Response
    {
        $news = $repository->findBy(
            ['isPublished' => true],
            ['createdAt' => 'DESC']
        );
        $pagination = $paginator->paginate(
            $news,
            $request->query->getInt('page', 1),
            10
        );
        return $this->render('news/news.html.twig', ['news' => $pagination]);
    }

    #[Route('/newnews', name: 'new_news')]
    public function addNew(EntityManagerInterface $manager, Request $request) {
        $news = new News();
        $form = $this->createForm(NewType::class, $news);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $news->setSlug($news->getName())
                 ->setCreatedAt(new \DateTimeImmutable())
                 ->setUpdatedAt(new \DateTimeImmutable())
                 ->setIsPublished(true)
                 ->setAuthor($this->getUser());
            $manager->persist($news);
            $manager->flush();
            $this->addFlash(
                'success',
                'La news "'.$news->getName().'" a bien été ajoutée'
            );
            return $this->redirectToRoute('news');
        }
        return $this->renderForm('news/newnews.html.twig', [
            'form' => $form
        ]);
    }
}
