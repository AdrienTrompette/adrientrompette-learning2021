<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Course;
use App\Form\CommentType;
use App\Repository\CommentRepository;
use App\Repository\CourseCategoryRepository;
use App\Repository\CourseRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CourseController extends AbstractController
{

    /**
     * @param CourseCategoryRepository $categoryRepository
     * @param CourseRepository $courseRepository
     * @return Response
     */
    #[Route('/courses', name: 'courses')]
    public function courses(CourseCategoryRepository $categoryRepository, CourseRepository $courseRepository): Response
    {
        $categories = $categoryRepository->findBy(
            [],
            ['name' => 'ASC']
        );
        $courses = $courseRepository->findBy(
            ['isPublished' => true],
            ['createdAt' => 'DESC']
        );


        return $this->render('course/courses.html.twig', [
            'categories' => $categories,
            'courses' => $courses
        ]);
    }


    /**
     * @param Course $course
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/course/{slug}', name: 'course')]
    public function course(Course $course, Request $request, EntityManagerInterface $manager): Response
    {
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $comment->setAuthor($this->getUser());
            $comment->setCourse($course);
            $comment->setCreatedAt(new \DateTimeImmutable());
            $comment->setIsPublished(true);
            $manager->persist($comment);
            $manager->flush();

            unset($comment);
            unset($form);
            $comment = new Comment();
            $form = $this->createForm(CommentType::class, $comment);

            $this->addFlash(
                'success',
                'Votre commentaire a bien été posté!'
            );
        }

        return $this->renderForm('course/course.html.twig', [
            'course' => $course,
            'form' => $form,
        ]);
    }
}
