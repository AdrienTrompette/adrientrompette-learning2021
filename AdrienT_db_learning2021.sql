-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 16 déc. 2021 à 20:44
-- Version du serveur :  5.7.31
-- Version de PHP : 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `learning2021`
--

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `rating` int(11) NOT NULL,
  `comment` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9474526CF675F31B` (`author_id`),
  KEY `IDX_9474526C591CC992` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=904 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `comment`
--

INSERT INTO `comment` (`id`, `author_id`, `course_id`, `created_at`, `rating`, `comment`, `title`, `is_published`) VALUES
(822, 2364, 763, '2021-12-13 17:48:38', 1, 'Quia ad velit dolores eum laborum ea. Illo temporibus molestias et aut nulla accusantium omnis. Dolorem dolorem cumque adipisci ea est et velit.', 'sint ut tempore', 0),
(823, 2375, 758, '2021-12-13 17:48:38', 3, 'Vero sit eos ipsum inventore quo commodi magni. Distinctio placeat doloribus quos. Nihil ea iste eum explicabo et blanditiis est laboriosam. Temporibus veritatis et ipsum odio molestias sit.', 'pariatur aut nostrum', 1),
(824, 2381, 743, '2021-12-13 17:48:38', 1, 'Consequatur eum et sequi similique omnis tempore voluptas. Molestias voluptates quaerat non quae harum. Soluta enim nulla temporibus.', 'omnis odio alias', 1),
(825, 2357, 751, '2021-12-13 17:48:38', 1, 'Fugit omnis eos aliquid libero modi sit labore autem. Quod maiores qui aut distinctio eos. Ex vitae amet maxime beatae.', 'voluptates quia amet', 1),
(826, 2395, 741, '2021-12-13 17:48:38', 4, 'Sunt ea natus nemo est est perspiciatis doloremque tenetur. Dolores minus ad sed eligendi. Rerum quia nihil placeat.', 'facilis quo sint', 1),
(827, 2368, 745, '2021-12-13 17:48:38', 1, 'Cupiditate magni alias aut qui ad quibusdam. Voluptate totam veritatis quia adipisci deleniti ducimus reprehenderit. Iste amet quia delectus voluptates voluptas adipisci quos. Expedita aut nemo at. Et quibusdam reiciendis tempora quia blanditiis omnis.', 'dolore eaque nobis', 0),
(828, 2372, 756, '2021-12-13 17:48:38', 4, 'Sint dolores et vitae quidem possimus amet tenetur. Illum sit et est est mollitia ut. Occaecati sunt qui voluptatem quaerat consequatur et ipsam ut. Dignissimos dolores consequatur laudantium temporibus.', 'nihil sunt ut', 1),
(829, 2388, 760, '2021-12-13 17:48:38', 5, 'Sed repellendus consequatur ut corrupti cumque consequatur sunt. Labore rerum omnis consequatur consequatur ex qui enim. Maiores aut voluptatum vitae.', 'rerum et consectetur', 0),
(830, 2385, 758, '2021-12-13 17:48:38', 5, 'Soluta a provident et id dolor. A itaque culpa qui distinctio velit. Quia dolores quidem aliquid odit rerum. Dignissimos velit nam aut praesentium qui et.', 'corrupti repudiandae omnis', 1),
(831, 2368, 752, '2021-12-13 17:48:38', 4, 'Nesciunt harum quod voluptas minus consequuntur eaque. Dolorem illum et maxime ut cupiditate. Facilis et quis in porro voluptatem dolorem aspernatur. Nemo cupiditate officiis ratione ad vero accusantium repellat.', 'incidunt saepe voluptatum', 1),
(832, 2357, 750, '2021-12-13 17:48:38', 4, 'Et quod sed nobis repudiandae incidunt tempore. Voluptatem sit harum molestiae unde consectetur nemo laborum. Amet aliquid voluptas esse et dolores.', 'aspernatur saepe dolores', 1),
(833, 2373, 762, '2021-12-13 17:48:38', 5, 'Facilis vel eos cum delectus rerum. Officiis esse qui et. Ut ut et quidem et odio soluta sit non.', 'magni porro eos', 1),
(834, 2388, 747, '2021-12-13 17:48:38', 5, 'Praesentium at porro corporis et illo voluptatem. Et qui aut quas vitae ut omnis. Cum tempore distinctio illo nam ullam fugit.', 'eveniet ea reprehenderit', 1),
(835, 2395, 762, '2021-12-13 17:48:38', 1, 'Amet ducimus perspiciatis quo ipsam nam autem repellat ducimus. Maxime cumque architecto illum suscipit quisquam. Unde culpa cupiditate excepturi sit sit recusandae totam. Alias aliquid aspernatur aut sit est blanditiis cum tempora.', 'amet ut rem', 1),
(836, 2354, 744, '2021-12-13 17:48:38', 3, 'Similique labore ut qui doloribus. Quibusdam tempore ab sit saepe. Doloremque iste porro illum sit animi molestiae consequuntur. Natus inventore sunt repellat sint nam ratione.', 'natus ab dolores', 1),
(837, 2354, 743, '2021-12-13 17:48:38', 2, 'Ut corporis rerum vero atque mollitia nisi fugiat. A reiciendis nemo nulla in sit error ipsa quas. Consequatur omnis dolores atque est soluta.', 'quibusdam sunt eius', 1),
(838, 2367, 740, '2021-12-13 17:48:38', 2, 'Expedita facilis nostrum velit et error. Ducimus quisquam nihil aliquam esse eum.', 'iusto eum ut', 1),
(839, 2390, 756, '2021-12-13 17:48:38', 5, 'Vel ut facere nobis labore occaecati voluptates vero tempore. Sint quo in animi nemo nihil.', 'quia voluptas praesentium', 0),
(840, 2368, 754, '2021-12-13 17:48:38', 5, 'Aut non nobis quae eaque quae amet dolor qui. Sint est aut explicabo. At voluptatem iure ut vel totam et similique non.', 'cum sunt voluptatem', 1),
(841, 2383, 747, '2021-12-13 17:48:38', 4, 'Ut quo corrupti occaecati. Consectetur quaerat facere numquam rerum quas facilis. Ex sed rerum veritatis aspernatur placeat consequatur. Velit sunt ut eos optio sint voluptas.', 'nulla tempore provident', 1),
(842, 2377, 745, '2021-12-13 17:48:38', 3, 'Facilis quia maiores id commodi. Quam et cum ut atque temporibus quidem deleniti. Nesciunt labore dolores autem aperiam velit non. Nihil odit vel unde.', 'exercitationem in soluta', 1),
(843, 2380, 754, '2021-12-13 17:48:38', 1, 'Et cum dolor sit dolorem nam quia praesentium. Sunt sit quidem quia vitae quo. Debitis sint quo sunt animi accusantium qui quasi.', 'maxime eum saepe', 1),
(844, 2377, 740, '2021-12-13 17:48:38', 3, 'Ratione est optio atque dolores nesciunt eos id ab. Voluptatem commodi non molestiae quas reprehenderit velit aut consequatur. Sint repellat praesentium tempora quaerat ut odit ad eos. In impedit atque voluptas cum.', 'libero autem ducimus', 1),
(845, 2379, 748, '2021-12-13 17:48:38', 4, 'Blanditiis sit vero velit. Et ea voluptatum tenetur laudantium. Laboriosam alias at nihil deleniti veritatis.', 'laudantium et maxime', 0),
(846, 2362, 742, '2021-12-13 17:48:38', 4, 'Delectus veritatis nulla at eligendi dolorem et. Ullam eum voluptatem enim dignissimos earum. Aliquam in in quae qui repellendus voluptatibus consequatur aut. Ad qui amet eos.', 'dignissimos est voluptatem', 1),
(847, 2370, 759, '2021-12-13 17:48:38', 1, 'Veritatis ipsum facere est aliquid. Sint libero hic molestiae et similique dicta. Deserunt est laudantium omnis.', 'explicabo reiciendis doloribus', 1),
(848, 2370, 749, '2021-12-13 17:48:38', 5, 'Nobis minima id quia reprehenderit minus. Numquam ducimus tempore quas.', 'sit dignissimos in', 1),
(849, 2351, 759, '2021-12-13 17:48:38', 3, 'Aut sunt reprehenderit est quis quo ut. Deleniti repudiandae incidunt voluptas vitae beatae. Nemo omnis animi ipsum dignissimos rem reiciendis. Voluptatibus rerum minus voluptatibus enim voluptatibus. Magnam illo et sit sit dolor ipsam est vero.', 'occaecati dolorum omnis', 1),
(850, 2359, 763, '2021-12-13 17:48:38', 3, 'Alias ipsum et voluptate delectus asperiores facere. Dolores eos quasi impedit.', 'quia delectus ipsam', 1),
(851, 2355, 752, '2021-12-13 17:48:38', 3, 'Quod debitis itaque qui officia voluptas et quia. Illo neque odio doloribus corporis velit. Velit magnam dolorem deserunt reprehenderit.', 'eligendi omnis minus', 1),
(852, 2392, 755, '2021-12-13 17:48:38', 1, 'Consequatur distinctio sit exercitationem qui sit. Inventore ut accusamus a deserunt tempora ipsam. Sit quidem at sequi nam. Optio ad facilis consequatur nobis sit quas.', 'saepe fugiat voluptatem', 1),
(853, 2392, 759, '2021-12-13 17:48:38', 5, 'Vel earum sint enim nobis pariatur qui. Pariatur deleniti asperiores ratione sapiente aut ut. Doloribus qui et magni voluptates. Minima commodi est laudantium.', 'beatae et eos', 1),
(854, 2349, 758, '2021-12-13 17:48:38', 5, 'Occaecati aut commodi voluptate voluptas rerum. Sed natus atque aut et maiores. Rerum est cumque beatae sed necessitatibus vero.', 'aut et exercitationem', 0),
(855, 2348, 762, '2021-12-13 17:48:38', 3, 'Et vitae magnam eos ut blanditiis et. Consequatur sed placeat consequuntur est sequi magnam. Repellendus qui sapiente dignissimos voluptatem.', 'voluptatum non veniam', 1),
(856, 2375, 762, '2021-12-13 17:48:38', 5, 'Dolores et molestias voluptas exercitationem molestiae sint deleniti. Non quia sit quidem.', 'rerum sed ut', 1),
(857, 2365, 762, '2021-12-13 17:48:38', 4, 'Rem quasi odio velit iste quia. Corrupti doloremque autem placeat ut iste ipsa fugit. Quia expedita adipisci aut aut sunt.', 'molestiae labore atque', 1),
(858, 2361, 744, '2021-12-13 17:48:38', 3, 'Officiis nihil temporibus optio. Ipsam maiores vitae corporis recusandae recusandae exercitationem.', 'laboriosam possimus odit', 1),
(859, 2345, 761, '2021-12-13 17:48:38', 2, 'Dolores magnam et quia sunt minima. Fugit tempore praesentium aut quia quo. Et porro autem consequatur exercitationem voluptatum.', 'voluptatum neque sed', 1),
(860, 2374, 759, '2021-12-13 17:48:38', 2, 'Qui voluptas alias aperiam laudantium maxime perferendis. Dolor quam dolores ex.', 'similique voluptates minima', 1),
(861, 2365, 747, '2021-12-13 17:48:38', 3, 'Esse non consequatur maiores et maiores fuga. Repellendus ea iusto non eaque. Quia odio eos natus aspernatur et vitae voluptatem. Qui quaerat eius voluptas labore.', 'iure officiis dolorum', 1),
(862, 2393, 752, '2021-12-13 17:48:38', 2, 'Quo sunt maxime vero dolores. Corporis debitis animi totam. Illo tenetur autem rem est ut. Consequuntur dolorem quisquam pariatur aut quia non commodi.', 'rerum voluptatem magni', 1),
(863, 2363, 756, '2021-12-13 17:48:38', 3, 'Dolores doloribus ipsum et qui aliquid enim sequi veritatis. Ut non a quidem quo velit aspernatur vel. Quo praesentium ea et. Nisi et consequatur sit quae omnis ullam.', 'omnis architecto tempora', 0),
(864, 2368, 761, '2021-12-13 17:48:38', 1, 'Fugiat adipisci quo et. Dicta sit officiis atque sed ratione dolor iste ut. Ea eius aut reprehenderit voluptas consequatur. Perspiciatis inventore excepturi ratione quasi.', 'iure ipsum repellat', 1),
(865, 2348, 739, '2021-12-13 17:48:38', 1, 'Voluptatem nihil sit est cumque asperiores. Voluptatibus non non iusto cumque provident.', 'inventore officiis quia', 0),
(866, 2358, 758, '2021-12-13 17:48:38', 1, 'Excepturi deleniti atque distinctio et accusantium at neque adipisci. Sed est cupiditate omnis ea at.', 'blanditiis eos recusandae', 1),
(867, 2379, 756, '2021-12-13 17:48:38', 3, 'Minus earum sed officiis. Et ullam error necessitatibus et inventore numquam qui. Et sint dolor laudantium voluptatem ipsa qui. Natus nemo aliquam modi repellat nihil. Et et et distinctio minima dolores.', 'perferendis autem in', 0),
(868, 2390, 757, '2021-12-13 17:48:38', 4, 'Aliquam sit voluptate praesentium consequatur fugiat quam. Vel maiores omnis itaque delectus quisquam. Libero veritatis incidunt ut nemo neque.', 'non tempore temporibus', 1),
(869, 2390, 739, '2021-12-13 17:48:38', 1, 'Et molestias qui assumenda ut necessitatibus inventore et. Alias inventore soluta et vero ipsa et occaecati.', 'et doloribus modi', 1),
(870, 2388, 752, '2021-12-13 17:48:38', 5, 'Ipsa vel officiis iste sint aliquid explicabo ducimus. Vel sunt veritatis quae saepe sunt optio ipsa sed. Optio voluptates mollitia adipisci natus unde et consequatur repellat. Est fugiat reiciendis maiores ut alias porro porro.', 'enim et dolores', 1),
(871, 2345, 743, '2021-12-13 17:48:38', 3, 'Magni reiciendis nihil consequatur quis earum qui. Sit illum corrupti quisquam autem. Est vitae assumenda est voluptatem veritatis dolore.', 'repudiandae et natus', 1),
(872, 2380, 740, '2021-12-13 17:48:38', 3, 'Ut laudantium neque nobis excepturi. Soluta omnis qui necessitatibus in distinctio. Ut vel soluta et.', 'iure et qui', 0),
(873, 2391, 759, '2021-12-13 17:48:38', 5, 'Cum vel aut eius sunt aperiam consectetur. Ullam est quidem sapiente voluptatem quasi vel incidunt. Aut facere aut consequatur rerum itaque nostrum. Est fugiat iure magnam dicta beatae.', 'sint aliquid ducimus', 1),
(874, 2384, 758, '2021-12-13 17:48:38', 4, 'Id eum consequuntur maiores tempore est. Qui rem delectus nam doloremque. Quis sunt doloremque et commodi qui. Perspiciatis temporibus itaque natus est id. Autem repellendus sed voluptatem sit sit assumenda.', 'et natus dolor', 1),
(875, 2372, 761, '2021-12-13 17:48:38', 2, 'Animi quo aliquid in ut ut. Non labore fuga hic perspiciatis. Unde ipsum dignissimos facere. Aut natus quasi aperiam recusandae quasi ut.', 'aut quis voluptates', 1),
(876, 2388, 743, '2021-12-13 17:48:38', 2, 'Sint eos aliquid nulla minus. Soluta repellat dolorum ipsa qui. Necessitatibus numquam nemo facere nulla et.', 'aliquid ea ut', 1),
(877, 2376, 750, '2021-12-13 17:48:38', 1, 'Magni laudantium et ea fuga voluptatem. Quo ratione ut odio enim ea rerum. Cupiditate ab similique cum illum voluptatem sed.', 'voluptas velit aliquid', 1),
(878, 2380, 752, '2021-12-13 17:48:38', 2, 'Et quo voluptatem quis quidem dolorem et dolorem. Rem autem ut aliquid qui aut sapiente. Quos ullam qui natus numquam harum dolor maiores.', 'repellendus voluptas laboriosam', 1),
(879, 2349, 747, '2021-12-13 17:48:38', 5, 'Velit quos culpa explicabo et quisquam eligendi. Et deserunt hic ut est. Incidunt unde voluptatum animi eum delectus et. Molestiae culpa eaque sequi aspernatur.', 'nam accusamus qui', 1),
(880, 2389, 761, '2021-12-13 17:48:38', 4, 'Autem asperiores maiores minus est totam. Autem molestias sint vero in qui et. Et sapiente et quod praesentium sapiente.', 'et in sed', 1),
(881, 2390, 760, '2021-12-13 17:48:38', 4, 'Voluptatem itaque cupiditate quia omnis. Et ea laborum est. Doloremque neque illum harum. Et voluptas est adipisci quasi fugit omnis aut cumque. Cupiditate sequi accusamus qui recusandae iusto veniam assumenda.', 'ipsum et molestias', 1),
(882, 2346, 755, '2021-12-13 17:48:38', 3, 'Rerum eius rerum vero est. Nam enim cumque dolorem est magni similique quisquam. Et molestiae neque rerum veritatis aut. Vel doloremque sint iste distinctio quisquam et aliquam quidem.', 'et at odit', 1),
(883, 2357, 761, '2021-12-13 17:48:38', 3, 'Et doloribus adipisci quo mollitia accusamus ipsum. Quis et vero vitae molestiae hic. Doloremque consequuntur consequatur earum quibusdam voluptatem. Accusamus est laudantium rerum nihil consequatur temporibus.', 'natus alias libero', 1),
(884, 2373, 740, '2021-12-13 17:48:38', 2, 'Aut rerum cupiditate inventore quo id quasi rerum. Omnis blanditiis quis eos.', 'maiores fugit necessitatibus', 1),
(885, 2354, 744, '2021-12-13 17:48:38', 3, 'Et voluptates voluptatibus dolorem accusamus doloribus laudantium. Sed dolorem modi exercitationem in. Et quisquam est voluptas culpa ex. Optio cum esse totam doloribus officia quos accusantium quidem.', 'explicabo fugit nulla', 1),
(886, 2353, 741, '2021-12-13 17:48:38', 5, 'Labore asperiores et aut et. Illo ut non hic minus necessitatibus. Eligendi saepe ullam quos architecto sint.', 'ipsum dolores sunt', 1),
(887, 2383, 752, '2021-12-13 17:48:38', 4, 'Ut voluptatem dolorem pariatur qui a id quia et. At rerum eos qui ab magni esse nihil. Suscipit debitis quis autem.', 'molestias laboriosam vero', 1),
(888, 2367, 755, '2021-12-13 17:48:38', 3, 'Error doloremque minus sit consequatur voluptas illo dolorem fuga. Itaque modi sit quo excepturi sequi. Totam ex aliquid ipsa.', 'molestias accusantium temporibus', 1),
(889, 2390, 763, '2021-12-13 17:48:38', 2, 'Voluptatum numquam quam corrupti nam molestiae sed. Eaque dolorem ut exercitationem aliquam. Neque id sit quia hic voluptate. Voluptatem eius ut vel perferendis.', 'aut aut porro', 1),
(890, 2363, 742, '2021-12-13 17:48:38', 2, 'Id libero non ipsum omnis ea vitae ex. Eos similique sed deleniti quisquam inventore commodi. Itaque aut ut molestias officia natus pariatur non necessitatibus.', 'dolores dolores facilis', 1),
(891, 2388, 755, '2021-12-13 17:48:38', 2, 'Laborum deserunt magni eum perferendis aut autem minima. Quisquam recusandae occaecati amet soluta reprehenderit. Est dolorum voluptate tempora. Nobis pariatur quia fuga praesentium dolores voluptatem.', 'enim consequatur reiciendis', 1),
(892, 2375, 744, '2021-12-13 17:48:38', 1, 'Molestiae rerum quasi nam qui. Quos perferendis recusandae a ducimus voluptatum provident eius facilis. Vero laborum eveniet assumenda impedit numquam consectetur cum.', 'consequatur vero voluptas', 1),
(893, 2374, 763, '2021-12-13 17:48:38', 3, 'Quia commodi commodi suscipit aspernatur laborum expedita neque. Et repellat non enim ipsa numquam et in. Sint omnis praesentium quos minima ut. Eum officia atque numquam voluptas quia.', 'enim sint quis', 1),
(894, 2379, 758, '2021-12-13 17:48:38', 4, 'Omnis qui voluptas et minima deleniti corrupti. Nobis exercitationem sunt ut labore occaecati. Repellat optio est et id ratione placeat. Quia ipsam culpa voluptas ratione et quibusdam.', 'ad nam voluptas', 1),
(895, 2383, 739, '2021-12-13 17:48:38', 3, 'Eius veritatis quo quo corrupti et architecto et fugiat. Iusto laborum error voluptas inventore eveniet. Ipsam ut et sint quia placeat odio quibusdam.', 'vel nesciunt optio', 1),
(896, 2347, 754, '2021-12-13 17:48:38', 2, 'Minus fuga ad quidem dolores ipsam eum laborum. Ducimus eveniet sit deserunt laboriosam. Nobis veritatis consequuntur enim ipsum quam. Iusto vitae ipsum rem consequatur animi at quidem similique. Earum adipisci harum delectus.', 'nihil excepturi ad', 1),
(897, 2363, 754, '2021-12-13 17:48:38', 2, 'Culpa quis nostrum et error. Ut odit facere accusamus necessitatibus ea.', 'aut velit dicta', 1),
(898, 2356, 748, '2021-12-13 17:48:38', 4, 'Vitae voluptatem qui tempora quisquam rem voluptas omnis. Eligendi harum dicta quia similique. Beatae facere deleniti adipisci autem optio repellendus vitae.', 'inventore amet rerum', 1),
(899, 2397, 748, '2021-12-13 17:48:38', 2, 'Fuga qui quidem consequuntur. Ullam mollitia nihil et in aut voluptatum sit quo. Aspernatur architecto delectus rerum voluptatem enim ullam quod.', 'voluptatibus qui rerum', 1),
(900, 2374, 741, '2021-12-13 17:48:38', 1, 'Dolore et eligendi sed ut veritatis tempora et. Amet accusantium consectetur minus aut facilis excepturi et quas. Qui doloremque et iusto. Enim voluptates tempora rerum.', 'ut consequatur facilis', 1),
(901, 2363, 741, '2021-12-13 17:48:38', 4, 'Provident autem architecto eligendi earum excepturi molestiae rerum. Qui consequatur consequatur ut cum. Omnis quis quibusdam praesentium odio.', 'hic qui porro', 1),
(902, 2397, 758, '2021-12-14 18:10:33', 4, 'Très bon', 'Bon cours', 1),
(903, 2398, 766, '2021-12-16 17:37:43', 12, 'cool', 'Cool544', 1);

-- --------------------------------------------------------

--
-- Structure de la table `course`
--

DROP TABLE IF EXISTS `course`;
CREATE TABLE IF NOT EXISTS `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `small_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` int(11) NOT NULL,
  `price` double NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `is_published` tinyint(1) NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `schedule` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `program` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `teacher_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_169E6FB912469DE2` (`category_id`),
  KEY `IDX_169E6FB95FB14BA7` (`level_id`),
  KEY `IDX_169E6FB941807E1D` (`teacher_id`)
) ENGINE=InnoDB AUTO_INCREMENT=767 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `course`
--

INSERT INTO `course` (`id`, `category_id`, `level_id`, `name`, `small_description`, `full_description`, `duration`, `price`, `created_at`, `is_published`, `slug`, `image`, `schedule`, `program`, `teacher_id`) VALUES
(739, 342, 190, 'Qui quasi.', 'Sit fugiat eveniet voluptatem omnis alias eligendi.', 'Rerum laborum sapiente dolorem quia ut. Cum omnis voluptates necessitatibus reiciendis possimus voluptatem. Voluptatem eum eos recusandae placeat aperiam. At et magnam molestiae et. Sunt autem harum nihil rerum. Et omnis beatae commodi eum. Dicta officia doloremque aut.', 180, 300, '2021-12-13 17:48:38', 1, 'qui-quasi', '1.jpg', 'Tuesday', '1.pdf', 15),
(740, 340, 190, 'Dolores et id.', 'Nihil id ut dolores fugit nihil. Quo molestiae animi consequuntur delectus minus.', 'Eligendi officia aut in velit in et. Consectetur voluptas rem cumque incidunt ut quaerat sed. Consequatur aut ut aut omnis aut. Quia fuga quidem rerum quo temporibus dolorem corrupti ut. Illo optio ut et quia aut consequuntur officia.', 300, 80, '2021-12-13 17:48:38', 1, 'dolores-et-id', '2.jpg', 'Thursday', '2.pdf', 10),
(741, 341, 189, 'Omnis necessitatibus cumque.', 'Et voluptate ad exercitationem cumque nemo earum eum. Quam quia ut ut.', 'Cupiditate quia explicabo provident veniam. Officia nulla reiciendis necessitatibus eligendi laudantium. Sed non sed quia laudantium nisi et. Rerum suscipit mollitia vel autem occaecati. Voluptatibus sint reprehenderit quam quia voluptatem illum.', 120, 340.5, '2021-12-13 17:48:38', 1, 'omnis-necessitatibus-cumque', '3.jpg', 'Tuesday', '3.pdf', 15),
(742, 339, 190, 'Eius magnam ut.', 'Corrupti modi accusamus excepturi vel. Suscipit et quaerat ipsa expedita optio eos sit.', 'Itaque expedita officiis eligendi est itaque laborum. Ut minus quo doloremque tempore natus eum. Libero fuga tenetur quod sapiente sed expedita. Necessitatibus voluptas ab atque perspiciatis recusandae non. Impedit numquam quia doloribus saepe. Soluta numquam iste eos est. Distinctio qui consequatur illum eveniet vitae a.', 120, 400, '2021-12-13 17:48:38', 1, 'eius-magnam-ut', '4.jpg', 'Tuesday', '4.pdf', 10),
(743, 337, 189, 'Eos vitae.', 'Quaerat vitae perspiciatis voluptas fugiat saepe itaque consequatur et.', 'In earum exercitationem eaque id asperiores. Et laboriosam eligendi qui rem. Fugit ut voluptatum nihil doloremque quisquam quasi. Voluptate eaque aut eveniet dolor doloremque mollitia.', 60, 200, '2021-12-13 17:48:38', 1, 'eos-vitae', '5.jpg', 'Wednesday', '5.pdf', 10),
(744, 342, 189, 'Error quidem sed.', 'Corporis dolorem rerum tenetur laboriosam. Qui quo et autem harum quibusdam et consequuntur sed.', 'Veritatis harum ipsum totam aut autem voluptatem vel. Sint iusto iusto a accusamus. Omnis numquam vel ducimus numquam. Asperiores occaecati et voluptatibus.', 180, 80, '2021-12-13 17:48:38', 1, 'error-quidem-sed', '6.jpg', 'Wednesday', '6.pdf', 9),
(745, 338, 191, 'Facere labore voluptatibus.', 'Voluptatem dolor ut enim cumque. In magni culpa alias libero ratione odio dolor.', 'Possimus expedita magni non commodi tenetur ipsum voluptatem. Modi non accusantium tempore laborum aperiam doloribus. Impedit iure eum magni sequi quod. Voluptatem sit eligendi voluptatem et aspernatur eius. Et aut nobis asperiores consectetur debitis sit illum. Fugit quasi natus minima totam est.', 360, 400, '2021-12-13 17:48:38', 1, 'facere-labore-voluptatibus', '7.jpg', 'Saturday', '7.pdf', 10),
(746, 337, 189, 'Fuga aperiam.', 'Quasi quod praesentium ex dolorem aliquam. Architecto non est sequi nesciunt cupiditate quis ducimus.', 'Vel aperiam omnis omnis aliquid aspernatur accusamus quaerat. Est quia doloribus minima aliquid dolores sed. Error itaque sit quaerat alias nam. Temporibus dolor occaecati quia. Nam aut ea aut aut odio. Sunt placeat perspiciatis mollitia voluptate. Est id nemo voluptates dolor quia vero laboriosam.', 300, 300, '2021-12-13 17:48:38', 1, 'fuga-aperiam', '8.jpg', 'Friday', '8.pdf', 15),
(747, 338, 189, 'Quia voluptatum.', 'Accusamus est in animi. Quia modi ut repellat autem.', 'Quis sint omnis assumenda libero expedita. Iste quod et quia odit. Beatae enim enim quia cupiditate nam voluptatibus eaque. Voluptatem rerum amet autem ex occaecati. Velit quisquam delectus consequuntur.', 360, 200, '2021-12-13 17:48:38', 1, 'quia-voluptatum', '9.jpg', 'Saturday', '9.pdf', 13),
(748, 343, 189, 'Necessitatibus eligendi.', 'Quos beatae quo voluptatem et.', 'Sed eaque deleniti tenetur nihil quidem. Nihil dolorum in omnis nulla. Autem inventore maxime excepturi inventore iure. Dolorem molestias ullam autem ut et. Tempore magnam maiores atque qui nam est.', 500, 80, '2021-12-13 17:48:38', 1, 'necessitatibus-eligendi', '10.jpg', 'Friday', '10.pdf', 12),
(749, 337, 192, 'Dolores sit illum.', 'Sapiente repellendus tempore voluptatum reprehenderit eos.', 'Libero sint consequatur delectus aut fugiat consequatur consequatur. Dolores dolor architecto optio aut. Inventore magnam fuga voluptatem. Voluptas ab illo voluptatem sunt voluptates. Expedita quis dolores eos numquam omnis. Nihil dolor dolorum sed ab dignissimos qui eaque. Et neque debitis rerum eos minima.', 120, 150, '2021-12-13 17:48:38', 1, 'dolores-sit-illum', '11.jpg', 'Monday', '11.pdf', 12),
(750, 338, 189, 'Autem architecto sapiente.', 'Ut illum et omnis et animi. Ipsum iste incidunt placeat atque.', 'Accusamus qui dolore ea eligendi. Enim dolorum dicta sequi harum unde quisquam. Rem earum ratione distinctio facere. Consectetur dolorem repellendus quia iste in. Est accusamus temporibus porro doloremque voluptatum vel aut ut.', 500, 300, '2021-12-13 17:48:38', 1, 'autem-architecto-sapiente', '12.jpg', 'Friday', '12.pdf', 11),
(751, 339, 189, 'Fugiat beatae.', 'Atque et omnis nemo vero consequuntur est. Ut iusto et mollitia omnis est.', 'Ad quia minima cupiditate fugit aliquam dolor qui. Ea explicabo laborum ea qui dolores sint expedita animi. Ipsam non quis est esse numquam. Blanditiis quibusdam quam hic maxime et ut. Ut delectus molestias facere accusantium laudantium aliquid doloremque.', 180, 300, '2021-12-13 17:48:38', 0, 'fugiat-beatae', '13.jpg', 'Friday', '13.pdf', 12),
(752, 342, 190, 'Eos odio quis.', 'Aliquid et ullam optio.', 'Ullam aut consequatur cum iure id ut perferendis exercitationem. Ut repellendus et quia in eligendi est dolor. Tempore est sed cupiditate quaerat voluptatem harum distinctio. Voluptatem amet neque iste necessitatibus beatae nihil a. Explicabo at ut minima id reprehenderit et esse. Illum omnis excepturi nihil quod mollitia occaecati.', 120, 120, '2021-12-13 17:48:38', 0, 'eos-odio-quis', '14.jpg', 'Friday', '14.pdf', 9),
(753, 337, 190, 'Soluta incidunt ab.', 'Ducimus ea sequi et possimus similique tempora. Eum est earum est facere eius.', 'Voluptatem voluptas maiores reiciendis magni tempora facere. Quis quas distinctio voluptas odit ipsa. Est possimus sint eaque occaecati doloribus sed aut. At eum nesciunt pariatur fuga cumque itaque. Impedit at qui commodi et.', 360, 400, '2021-12-13 17:48:38', 1, 'soluta-incidunt-ab', '15.jpg', 'Thursday', '15.pdf', 10),
(754, 338, 190, 'Eos rerum eum.', 'Laudantium ut minima repellendus nisi quae aspernatur. Dolorum iusto rerum a sed inventore voluptatem laboriosam doloremque.', 'Sint ipsam aspernatur et voluptatem eos. Suscipit cupiditate ducimus sunt non perferendis voluptatem sapiente. Harum minima iusto neque quis ut tenetur quo expedita. Sit vel et aut nemo. Dolorem facilis aut voluptates quaerat sit earum aut. Esse iusto vitae ipsum quasi laudantium. Vel quas doloremque repudiandae est voluptate inventore.', 300, 200, '2021-12-13 17:48:38', 1, 'eos-rerum-eum', '16.jpg', 'Thursday', '16.pdf', 15),
(755, 341, 190, 'In sed provident.', 'Natus id ratione at sint illum modi. Laudantium dolorum ad labore minus.', 'Ratione sed nam est voluptates aut. Adipisci est consequatur corporis aut placeat quis beatae. In iure odit nemo sit voluptatem voluptas et. Corrupti ut vel ipsum quibusdam exercitationem labore. In veniam doloribus aut officiis in voluptas. Aut ratione atque commodi minima adipisci id.', 60, 250, '2021-12-13 17:48:38', 1, 'in-sed-provident', '17.jpg', 'Saturday', '17.pdf', 11),
(756, 339, 191, 'Voluptatem id qui.', 'Impedit quia necessitatibus harum sint.', 'Sed pariatur alias quia velit vel. Molestiae non voluptas voluptatem at et quam pariatur. Eius laudantium saepe ut maiores ipsum quidem numquam. Voluptas nostrum amet voluptatem eius distinctio quasi voluptas ut. Sit reprehenderit fugiat voluptatem voluptatibus nam alias.', 180, 340.5, '2021-12-13 17:48:38', 1, 'voluptatem-id-qui', '18.jpg', 'Saturday', '18.pdf', 16),
(757, 341, 191, 'Incidunt quas.', 'Sit dolorem vitae voluptas est id qui quidem.', 'Sequi aut amet perspiciatis quis similique voluptatum eum. Consequatur est voluptas laudantium iste laboriosam. Unde ad officiis sunt laudantium at ipsam voluptas. Id ipsam molestiae voluptatem recusandae et. Possimus vero dolore delectus voluptas ullam et. Beatae rerum dicta ut rerum quae.', 500, 250, '2021-12-13 17:48:38', 1, 'incidunt-quas', '19.jpg', 'Sunday', '19.pdf', 9),
(758, 340, 189, 'Aut qui.', 'Odio eos numquam deleniti.', 'Est hic a quidem suscipit optio iusto animi ipsa. Sit necessitatibus illo aspernatur architecto. A possimus magnam hic quae fugit in. Earum aut rerum assumenda nostrum perspiciatis dolorum. Autem voluptates alias et quibusdam. Et aspernatur eos cupiditate. Porro quia eos et est non qui qui.', 60, 300, '2021-12-13 17:48:38', 1, 'aut-qui', '20.jpg', 'Friday', '20.pdf', 9),
(759, 341, 191, 'Quis est.', 'Rem eum dolore aut. Vero ex distinctio impedit in magni cumque animi sit.', 'Qui maxime maiores dolorem perferendis. Et architecto est a nemo voluptatem maiores error. Repudiandae in aut qui modi quia fugit. Ea doloremque illo nam fuga consectetur dolor quod consectetur.', 120, 120, '2021-12-13 17:48:38', 1, 'quis-est', '21.jpg', 'Monday', '21.pdf', 13),
(760, 337, 189, 'Odio architecto.', 'Esse pariatur culpa perferendis illum nemo. Quae ipsam consectetur aut sed molestias nihil.', 'Molestias fugit ullam repellat assumenda aut quaerat. Praesentium non eligendi aut qui est enim. Dolor temporibus ea maxime aut. Deserunt molestias a laudantium consectetur. Voluptatem ea eos tenetur neque architecto veniam.', 180, 200, '2021-12-13 17:48:38', 1, 'odio-architecto', '22.jpg', 'Thursday', '22.pdf', 13),
(761, 337, 191, 'Ut et placeat.', 'Nisi iure et et adipisci.', 'Minus omnis ipsa voluptatem non dolore recusandae atque. Nisi tempore suscipit veritatis quasi omnis quas. Explicabo vero est voluptatem et blanditiis unde blanditiis temporibus. Quis ut sunt odio consectetur itaque. Quod illum a suscipit est rerum.', 60, 120, '2021-12-13 17:48:38', 1, 'ut-et-placeat', '23.jpg', 'Thursday', '23.pdf', 16),
(762, 338, 190, 'Expedita in.', 'Accusantium voluptas voluptatibus nisi iste. Mollitia et officiis est quidem dolor unde ullam.', 'Est facere earum quas odio. Explicabo voluptatem dolorem deserunt aliquid. Ipsam voluptatem qui minima placeat sit hic distinctio. Fuga quam non consequatur blanditiis sint nihil aut. Assumenda saepe omnis natus in. Sit dolorum eveniet aut dolore totam.', 200, 300, '2021-12-13 17:48:38', 1, 'expedita-in', '24.jpg', 'Wednesday', '24.pdf', 16),
(763, 342, 190, 'Aliquid perferendis.', 'Dignissimos voluptatum mollitia qui qui repellat id.', 'Hic deserunt quasi ut. Magnam reiciendis ad et in in. Quisquam qui autem vitae illo corrupti tempore. Soluta voluptas excepturi adipisci pariatur vel soluta.', 120, 80, '2021-12-13 17:48:38', 1, 'aliquid-perferendis', '25.jpg', 'Thursday', '25.pdf', 12),
(764, 339, 190, 'Ipsa nemo tempora.', 'Aliquid fugiat nesciunt quis deserunt quo. Iusto nihil voluptatum eos alias quo.', 'Vel vel saepe unde. Quos aut quo atque et eos. Ipsum est debitis molestias architecto. Praesentium itaque ipsa quo. Rem quas ea in deleniti magni placeat voluptas. Aut at velit ut dicta.', 120, 400, '2021-12-13 17:48:38', 1, 'ipsa-nemo-tempora', '26.jpg', 'Thursday', '26.pdf', 11),
(765, 339, 190, 'Symfony 5', 'Base de symfony 5', '<p><strong>SYMFONY5</strong></p>\r\n\r\n<p><span style=\"color:#2980b9\">D&eacute;couvrez les fonctionnalit&eacute;s de base et apprenez le framework symfony 5</span></p>', 300, 150, '2021-12-15 15:50:47', 1, 'symfony-5', 'maxresdefault-61ba0df74a34b363059250.jpg', '27-12 > 31-12', 'application-symfony-61ba0df74be71809605037.pdf', 11),
(766, 339, 191, 'VueJS 3', 'Base de VueJS 3', '<p>D&eacute;couvrez et ajoutez les fonctionnalit&eacute;s de base et l&#39;utilisation de VueJS dans vos projets</p>', 300, 200, '2021-12-16 17:43:30', 1, 'vuejs-3', 'vg0v5vm9a0c1ix6mdp9s-61ba0f687534b567143517.png', '27-12 > 31-12', 'cahier-charges-2021-61ba0f6876d7b644403607.pdf', 12);

-- --------------------------------------------------------

--
-- Structure de la table `course_category`
--

DROP TABLE IF EXISTS `course_category`;
CREATE TABLE IF NOT EXISTS `course_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=344 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `course_category`
--

INSERT INTO `course_category` (`id`, `name`, `description`) VALUES
(337, 'Artisanat', 'Quia laborum non numquam possimus molestiae.'),
(338, 'Bien être', 'Perspiciatis harum vitae sed quae autem repellat.'),
(339, 'Webdeveloper', 'Iusto qui cum praesentium qui nihil accusantium.'),
(340, 'Langues', 'Qui aut nostrum asperiores quibusdam.'),
(341, 'Technique', 'At eos et tenetur laboriosam laudantium possimus rerum.'),
(342, 'Informatique', 'Sequi harum qui dicta ea deleniti sunt.'),
(343, 'Pédagogique', 'Adipisci ipsum sed quia sit voluptates facere recusandae.');

-- --------------------------------------------------------

--
-- Structure de la table `course_level`
--

DROP TABLE IF EXISTS `course_level`;
CREATE TABLE IF NOT EXISTS `course_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prerequisite` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=193 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `course_level`
--

INSERT INTO `course_level` (`id`, `name`, `prerequisite`) VALUES
(189, 'Débutant', 'Certificat de base'),
(190, 'Confirmé', 'Connaissances de base'),
(191, 'Spécialisé', 'Connaissances avancées'),
(192, 'Expert', 'Pratique professionnelle et expertise');

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20211213174930', '2021-12-13 17:49:41', 174);

-- --------------------------------------------------------

--
-- Structure de la table `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1DD39950F675F31B` (`author_id`)
) ENGINE=InnoDB AUTO_INCREMENT=610 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `news`
--

INSERT INTO `news` (`id`, `author_id`, `name`, `created_at`, `updated_at`, `content`, `image`, `is_published`, `slug`) VALUES
(551, 2381, 'quisquam qui e', '2021-12-13 19:41:57', '2021-12-13 19:41:57', 'Quis est odit autem deserunt. Asperiores natus voluptates eveniet totam minima sed. Qui dolor aut voluptas voluptas omnis. Nostrum ducimus est vero qui voluptatibus dolores.', '01.png', 1, 'quisquam-qui-e'),
(552, 2375, 'exercitationem laborum accusamus', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Totam repellendus officiis totam aut. Occaecati porro sed fuga laborum nemo. Et impedit non delectus vitae perferendis nobis. Vitae est fuga saepe recusandae. Est omnis quis autem facere autem itaque. Commodi totam repellat placeat aut.', '02.png', 1, 'exercitationem-laborum-accusamus'),
(553, 2355, 'aut quo et', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Laudantium consequuntur esse corrupti dolore. Quia iure voluptatem odio voluptatem ipsum. Libero esse dolor sint atque laudantium dignissimos et. Vel dolorum eum et assumenda veniam explicabo. Atque sit repellendus aut eligendi unde. Qui totam ipsa et voluptas quod totam incidunt.', '03.png', 1, 'aut-quo-et'),
(554, 2360, 'doloremque fugit natus', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Quod molestias modi quia omnis fugiat atque. Quia ipsam in quibusdam aut. Et aut sunt et sit ea eum culpa. Accusantium in eos ducimus a. Voluptatem nobis non incidunt nulla. Molestias amet reprehenderit dignissimos tenetur.', '04.png', 1, 'doloremque-fugit-natus'),
(555, 2396, 'beatae labore ea', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Ab vel nesciunt consequatur. Error occaecati est in distinctio quis sed. Et maiores eos animi earum inventore fugiat velit. Et est pariatur ea alias. Sunt fugiat ut vero aut velit suscipit. Exercitationem temporibus magni eaque quaerat quidem. Placeat molestiae minima excepturi eaque voluptatem quia.', '05.png', 1, 'beatae-labore-ea'),
(556, 2355, 'sit error quam', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Sed qui earum dolorem voluptate quo repellat. Non facere eaque corrupti blanditiis quisquam magni harum. Quis commodi itaque porro similique quis quos. Veritatis repellat debitis minus autem fugiat. Qui iste molestiae iusto magnam assumenda.', '06.png', 1, 'sit-error-quam'),
(557, 2383, 'quia exercitationem omnis', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Repudiandae repellat consectetur quam harum ex perferendis. Pariatur assumenda sed et in voluptates doloribus. Porro neque est vel molestiae quas quia. Rerum quod et quasi totam corrupti aut voluptatem. Suscipit aut praesentium unde accusantium ipsum nihil omnis neque.', '07.png', 1, 'quia-exercitationem-omnis'),
(558, 2393, 'ut illo asperiores', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Vel sit sapiente sapiente ut quos. Atque voluptatem placeat est est qui. Aut unde at maxime. Consequuntur est recusandae velit sed magnam. Nam aliquam omnis rerum blanditiis magnam corporis doloremque magnam.', '08.png', 1, 'ut-illo-asperiores'),
(559, 2397, 'ipsum saepe laboriosam', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Tempora est repudiandae excepturi accusantium voluptas sequi. Aut vel perspiciatis quaerat eligendi. Mollitia error veritatis sint modi voluptas voluptatem. Qui rerum quis corrupti labore et incidunt provident quasi.', '09.png', 0, 'ipsum-saepe-laboriosam'),
(560, 2394, 'id ut quis', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Non earum ea ea id sunt asperiores molestiae. Consectetur voluptate recusandae dolorem qui. Qui officia et ad repudiandae est eligendi. Expedita qui est nihil. Unde sit ut porro provident doloremque possimus maxime.', '010.png', 1, 'id-ut-quis'),
(561, 2350, 'quibusdam excepturi quod', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Iusto earum numquam quia ipsa aut. Accusantium possimus sint ullam porro debitis quis aut. Inventore dolore voluptate itaque fuga accusantium molestias omnis animi. Non dicta rerum id.', '011.png', 1, 'quibusdam-excepturi-quod'),
(562, 2364, 'iure non qui', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Debitis quia et repellat. Autem et iure quia quia odio. Qui et necessitatibus voluptatum aut labore exercitationem ut. Qui est quia blanditiis quaerat ea vel est. Quia nobis placeat et. Sequi quibusdam voluptas aliquam quibusdam consequatur atque.', '012.png', 1, 'iure-non-qui'),
(563, 2390, 'velit enim quasi', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Fugiat occaecati distinctio iste alias. A et aliquid ipsum consectetur architecto sint labore. Quia reiciendis fuga eos tempore nisi consequuntur optio. Animi nihil occaecati qui corrupti similique omnis.', '013.png', 1, 'velit-enim-quasi'),
(564, 2378, 'eveniet aliquid tempore', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Fugiat quo aut quis. Cumque molestiae modi consequatur nihil culpa ducimus. Nisi qui voluptatem corporis accusantium odit rerum minus. Blanditiis debitis facere molestiae dolores eveniet. Ratione quod alias sed qui rerum. In architecto sed itaque. Est quia dolores quidem dignissimos sed.', '014.png', 1, 'eveniet-aliquid-tempore'),
(565, 2381, 'laborum rem et', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Ex quia hic deleniti vitae. Voluptas hic rerum eaque hic veritatis eum. Consequatur voluptatibus modi optio facilis enim. Sed tempore dolores alias assumenda voluptatem reprehenderit. A rem velit omnis hic voluptatem quas. Cupiditate maxime sed nemo sint.', '015.png', 1, 'laborum-rem-et'),
(566, 2389, 'doloremque pariatur impedit', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Error officiis repellat esse totam autem unde quasi. Dolorem non quis qui animi nesciunt a vel. Eaque excepturi blanditiis voluptatem quia. Aut aut dolore itaque aut.', '016.png', 0, 'doloremque-pariatur-impedit'),
(567, 2350, 'ut et omnis', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Labore velit dicta sit omnis. Vel perspiciatis laudantium eum voluptatum quisquam vitae. Temporibus quaerat similique sint. Recusandae illum nihil harum rem alias.', '017.png', 1, 'ut-et-omnis'),
(568, 2395, 'nostrum minima veritatis', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Debitis placeat repudiandae ut nostrum. Ut aliquid voluptas suscipit reiciendis quas mollitia vero. Fuga quia beatae voluptas aut. Quo asperiores voluptatibus autem similique illum. Ut ut vitae fuga voluptate. Dolores reiciendis ad natus incidunt enim commodi consequatur. Ut aut ipsa similique possimus qui non aut eos.', '018.png', 1, 'nostrum-minima-veritatis'),
(569, 2354, 'consectetur quam sed', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Autem error aut vitae qui. Vero voluptatem est id ab voluptatem provident est. Sunt sapiente magni quidem nobis est. Quisquam et autem corporis odio. Quia saepe delectus possimus id est. Rerum tempora est vel. Eaque ipsa dolor nobis ipsa ea velit facere.', '019.png', 1, 'consectetur-quam-sed'),
(570, 2392, 'sed dolor blanditiis', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Beatae voluptatem saepe et atque. Quasi eos consequatur non minima vel sit. Ratione exercitationem error optio nulla porro sit. Deleniti voluptatem et ut cum. Vero dolor et nobis et.', '020.png', 1, 'sed-dolor-blanditiis'),
(571, 2380, 'corrupti hic dolorum', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Molestiae vero voluptatum id eum sint voluptas natus. Odit itaque hic a. Esse impedit quasi quo eum aut animi similique. A illum sint ullam accusamus.', '021.png', 1, 'corrupti-hic-dolorum'),
(572, 2361, 'illum rerum culpa', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Vero quos cum consequuntur veritatis. Numquam quo ad qui quidem aut aspernatur vitae et. Illo consequatur harum dolore rem laboriosam velit ut non. Doloribus earum eaque sunt asperiores voluptatum cupiditate exercitationem omnis. Similique non qui et possimus quia officia.', '022.png', 1, 'illum-rerum-culpa'),
(573, 2373, 'qui et et', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Iste eum saepe totam blanditiis. Quo impedit id quia. Excepturi tempore eos ea hic molestiae accusamus labore. Iusto eaque explicabo delectus ducimus aut. Illum sequi nostrum quibusdam non eaque.', '023.png', 1, 'qui-et-et'),
(574, 2350, 'earum reiciendis tempora', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Incidunt excepturi quas repellendus ducimus. Exercitationem qui illo voluptas maxime. Sequi aspernatur reiciendis quod aut earum culpa. Omnis eos cumque saepe expedita enim consequatur aperiam voluptatem. Ipsa magnam corrupti maxime et porro. Consequatur quaerat aut ratione in alias amet odit.', '024.png', 0, 'earum-reiciendis-tempora'),
(575, 2353, 'reprehenderit nulla nostrum', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Ipsam velit sequi nemo. Nostrum voluptatum dolorem atque officiis. Voluptas quia id unde quo qui est accusantium. Quia doloribus ullam et magni accusantium. Ut voluptate enim sequi quo impedit dolores perspiciatis ut. Impedit non sunt vel facilis vel excepturi repellat nisi.', '025.png', 0, 'reprehenderit-nulla-nostrum'),
(576, 2390, 'et enim dolores', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Et asperiores provident illo. Asperiores repellendus aut perferendis accusamus iure cum asperiores. Magni quas quia nihil aspernatur. Blanditiis et aliquam consequatur molestiae molestiae ipsum soluta.', '026.png', 1, 'et-enim-dolores'),
(577, 2368, 'quam a aut', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'In accusantium itaque quod vero amet blanditiis nesciunt. Et omnis possimus quis qui. Id vel quam modi eligendi ratione est et sit. Eum sit quibusdam nemo labore fuga asperiores. Sit consequatur aut id sit. Et est exercitationem aliquam beatae. Vel qui nostrum assumenda sed.', '027.png', 1, 'quam-a-aut'),
(578, 2349, 'natus nemo alias', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Sapiente minima officia amet iste tempora velit id. Qui animi eius minus qui natus. Iste suscipit ipsam ullam. Officiis omnis fuga dolorem quia consectetur. Enim iste quidem soluta quas vitae architecto sint. Rerum nihil voluptas molestias. Distinctio possimus libero quia sequi commodi fugiat.', '028.png', 0, 'natus-nemo-alias'),
(579, 2377, 'laborum eius ab', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Ut provident et quisquam ut doloremque eius voluptatibus. Quisquam rerum praesentium voluptatem fugit et voluptates ut ducimus. Cupiditate nesciunt dolorum sed unde ut ea veritatis. Inventore non in accusamus quas. Et molestiae pariatur eos rerum excepturi suscipit modi. In est aut et quod quo est.', '029.png', 1, 'laborum-eius-ab'),
(580, 2371, 'iusto et quas', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Rem libero in vitae illo quo rerum iusto quia. Et sed tempore ut quo. Dignissimos possimus voluptatem illo voluptatibus minima adipisci quam maxime. Corporis explicabo atque nihil.', '030.png', 1, 'iusto-et-quas'),
(581, 2359, 'sed debitis ad', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Qui laboriosam ab ut occaecati deleniti. Repudiandae ut saepe aut molestias veritatis et. In nulla nisi impedit molestiae dolores similique. Doloribus repellendus vero magni et quo est dolore. Recusandae ducimus veniam quam. Perspiciatis alias omnis neque assumenda suscipit perferendis et. Voluptas et dolores ut veritatis similique cupiditate.', '031.png', 0, 'sed-debitis-ad'),
(582, 2355, 'est quia non', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Earum sint excepturi aliquid est sint itaque culpa nemo. Fugit natus eveniet sit libero. Ullam exercitationem dolorum doloribus corrupti. Laboriosam voluptatum eos non aperiam iure. Et quis corporis modi iste alias. Qui ullam qui magni qui aut dolor officiis.', '032.png', 1, 'est-quia-non'),
(583, 2352, 'a et voluptatibus', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Harum ad maxime asperiores. Quo sint incidunt veniam. Recusandae pariatur voluptatem est fuga qui dolores non aperiam. Fugiat vitae nihil sit qui at. Et cum molestiae sit alias eum exercitationem ullam unde. Soluta dolore ipsam quo dolores. Ut omnis mollitia suscipit ullam ut corporis totam consectetur. Vero blanditiis labore qui cupiditate.', '033.png', 1, 'a-et-voluptatibus'),
(584, 2362, 'porro quidem officiis', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Non ad quia porro minus ad inventore voluptates. Quia sunt quasi eius quia excepturi. Modi occaecati nostrum non quia ab fugiat. Nam blanditiis tempore fuga reprehenderit explicabo.', '034.png', 0, 'porro-quidem-officiis'),
(585, 2371, 'quia omnis iste', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Occaecati molestias ad doloremque nisi alias earum. Aut magni facilis quo qui aliquid repudiandae commodi. Labore cum velit libero quibusdam. Explicabo porro repellendus nisi velit aut vitae ducimus vel. Molestias non porro voluptatem debitis. Et maxime dolores fuga architecto. Praesentium dolorem cupiditate a tenetur saepe.', '035.png', 1, 'quia-omnis-iste'),
(586, 2378, 'sint laudantium molestiae', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Distinctio et quia rem quis expedita. Eveniet velit consequatur enim excepturi non vitae in. Quibusdam unde itaque omnis debitis. Quia doloremque rerum reiciendis fuga dolores enim et. Harum enim facere ipsam et optio quaerat. Ipsam voluptatem minus sunt atque. Consequuntur placeat odio accusantium quis inventore voluptas ut.', '036.png', 1, 'sint-laudantium-molestiae'),
(587, 2385, 'qui cupiditate nemo', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Eos autem omnis doloremque tempore et ullam. A architecto vitae sit cupiditate accusamus. Ut qui perferendis placeat consequatur eligendi et modi illo. Ea id necessitatibus harum quisquam eos temporibus. A quia non dolorem consequatur laboriosam quia ut. Autem ut tempore fugiat mollitia. Tenetur iste veniam doloremque et.', '037.png', 1, 'qui-cupiditate-nemo'),
(588, 2387, 'blanditiis recusandae consectetur', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Sit recusandae et id officiis laudantium commodi et. Illo accusamus quam possimus consequatur. Corrupti recusandae eius corrupti possimus molestiae eum at. Consequuntur aliquid quia blanditiis nesciunt consequatur voluptatem possimus.', '038.png', 1, 'blanditiis-recusandae-consectetur'),
(589, 2365, 'voluptas possimus consequatur', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Minus quae totam consequuntur temporibus. Mollitia nihil esse dolores at beatae. Consectetur neque modi praesentium nobis est. Reiciendis doloremque consequatur magnam facilis repudiandae. Et quas illum quia fuga soluta aut. Consectetur blanditiis facilis hic molestiae voluptas laborum pariatur perspiciatis. Distinctio perspiciatis nam dolorem velit iure.', '039.png', 1, 'voluptas-possimus-consequatur'),
(590, 2393, 'minima dolor nobis', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Mollitia consequuntur autem aut et dignissimos aspernatur. Vero qui provident beatae amet molestiae modi velit. Eum nostrum voluptas est et vitae. Illum at assumenda voluptatem a.', '040.png', 1, 'minima-dolor-nobis'),
(591, 2352, 'reiciendis adipisci recusandae', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Odio neque et occaecati tenetur eveniet et adipisci. Aut ex omnis doloremque. Facilis asperiores sed quod eos ut repellendus qui qui. Voluptas qui minus delectus aut recusandae. Ex earum saepe possimus ut. Voluptatem nulla perspiciatis porro dolorem repellendus.', '041.png', 1, 'reiciendis-adipisci-recusandae'),
(592, 2367, 'vel et nam', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Corporis tempora ipsum quae voluptatem omnis. Sint non dolore harum debitis. Animi sint commodi magni enim voluptatem optio adipisci. Expedita quo rerum omnis nihil repellat.', '042.png', 1, 'vel-et-nam'),
(593, 2395, 'quisquam cumque quam', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Suscipit consequatur eveniet sit voluptatum qui iste et ab. Ut amet esse qui beatae rerum voluptatum. Suscipit quo voluptatum expedita non alias veniam rerum. Blanditiis fugit ea voluptas quos. Dolorem qui voluptatem perferendis.', '043.png', 1, 'quisquam-cumque-quam'),
(594, 2364, 'eos voluptatem velit', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Et quo fuga qui laudantium. Quisquam et mollitia libero dolor et. Molestiae ea aspernatur expedita et sint voluptatem tempore. Aperiam quasi aut rerum doloribus saepe similique. Aut voluptatem ullam rerum eius maiores aut voluptates.', '044.png', 1, 'eos-voluptatem-velit'),
(595, 2371, 'architecto tenetur nisi', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Praesentium aut enim aut aliquid tempore velit est. Praesentium nesciunt qui placeat fugiat. Est libero vel voluptatem harum. Explicabo quo rem et beatae.', '045.png', 1, 'architecto-tenetur-nisi'),
(596, 2358, 'facere maiores labore', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Asperiores ut et est odio amet. Voluptas ab quia et modi architecto. Nihil ut et necessitatibus cumque ea voluptatem dolorem. Sunt provident quo quis sint voluptas. Id voluptate praesentium sit ratione tenetur minus. Aut et molestias quasi corporis omnis voluptatibus. Aut sed dolor et tempore aut possimus.', '046.png', 1, 'facere-maiores-labore'),
(597, 2353, 'alias voluptatem unde', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Sed molestiae error et perferendis qui quibusdam. Non sit autem dolorem impedit sit. Eligendi beatae dicta qui ad quia magni vero dignissimos. Numquam et nemo sed eos. Voluptatem sed delectus voluptas repellendus eius officia ea dolor.', '047.png', 1, 'alias-voluptatem-unde'),
(598, 2368, 'culpa ut et', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Neque iste temporibus dolorum amet quidem dolores explicabo perferendis. Optio quae ut illo laboriosam. Autem illo assumenda quas quo aut. Deserunt ea tempora consequuntur debitis. Vel maiores asperiores reprehenderit qui quae modi ipsa voluptas. Similique adipisci eveniet et et deserunt.', '048.png', 0, 'culpa-ut-et'),
(599, 2397, 'debitis molestias error', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Sequi vitae voluptate minima inventore officia vitae. Totam a ullam voluptas qui ex. Et eaque omnis voluptatem cupiditate et optio. Magni et nisi iste qui corporis assumenda iusto. Est occaecati veritatis qui iusto nostrum.', '049.png', 1, 'debitis-molestias-error'),
(600, 2360, 'eius suscipit libero', '2021-12-13 17:48:38', '2021-12-13 17:48:38', 'Maxime vel consequatur totam rem non vel sit. In distinctio qui et id exercitationem nihil amet. Unde eius inventore facilis est nostrum voluptates. Veritatis sunt repellat esse laboriosam aut voluptatem.', '050.png', 1, 'eius-suscipit-libero'),
(601, 2395, 'Nouvelle version de Symfony 5', '2021-12-15 16:27:39', '2021-12-15 16:27:39', 'In ultricies lacus a mi mollis viverra. Morbi vestibulum nibh nisl, sed molestie odio consequat quis. Morbi vel neque non lectus porttitor laoreet sed id ex. In in euismod dolor. Sed laoreet suscipit leo vel ullamcorper. Integer varius velit nec nulla porttitor fringilla. Sed dolor nunc, auctor sed purus ultricies, consectetur scelerisque nunc.', 'symfony-61ba14fbb33d8426676468.png', 1, 'nouvelle-version-de-symfony-5'),
(602, 2395, 'Nouveauté javascript', '2021-12-15 16:28:03', '2021-12-15 16:28:03', 'Pellentesque dapibus efficitur orci eget viverra. Etiam malesuada dolor pulvinar purus finibus, ac bibendum quam interdum. Donec suscipit feugiat nunc, eu venenatis nibh consequat quis. Quisque sodales augue sit amet metus auctor eleifend. Mauris mattis vestibulum ligula, quis tristique tortor hendrerit id. Sed at ligula semper, fermentum augue ac, finibus purus.', 'js-61ba15a875d2f999193090.png', 1, 'nouveaute-javascript'),
(603, 2395, 'Nouvelle version de PHP', '2021-12-16 08:55:33', '2021-12-16 08:55:33', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent quis malesuada turpis, sit amet tincidunt quam. Aenean suscipit ut lectus quis malesuada. Integer non nisi massa. Duis gravida cursus sapien. Curabitur malesuada pharetra augue sit amet tristique. Suspendisse faucibus nisi eu sapien vulputate, laoreet aliquam nulla dictum. Pellentesque dapibus efficitur orci eget viverra.', 'php-logo-svg-61ba16a3f11fc432705890.png', 1, 'nouvelle-version-de-php'),
(604, 2364, 'Nouvelle version de Wamp', '2021-12-16 16:19:35', '2021-12-16 16:19:35', 'Lorem ipsum dolor solum. Aliquam venenatis leo id dui aliquam, et malesuada quam gravida. Proin condimentum pharetra justo. Nunc sed ligula ut augue maximus tincidunt id hendrerit sapien. Phasellus sit amet aliquet diam, et efficitur dui.', '220px-wampserver-logo-61ba1e80d69ae794261630.png', 1, 'nouvelle-version-de-wamp'),
(605, 2397, 'Lorem ipsum', '2021-12-16 14:56:04', '2021-12-16 14:56:04', 'Proin eu dignissim nulla, malesuada lobortis elit. Vestibulum a mauris ac mauris lacinia scelerisque et ac enim. Quisque tempor eu lorem condimentum eleifend. In tincidunt, sem nec rhoncus lobortis, nisi ligula vulputate turpis, id scelerisque nisl mi et leo. Sed in scelerisque turpis. Duis posuere, nunc eu malesuada ornare.', 'pexels-julius-silver-753626-61bb5384ad017507817517.jpg', 1, 'Lorem ipsum'),
(606, 2397, 'Jardinerie', '2021-12-16 16:55:12', '2021-12-16 16:55:12', 'Une jardinerie est un magasin de plus de 1 000 m2[réf. nécessaire] spécialisé dans la vente aux particuliers de végétaux et de toutes les fournitures, outillages et matériaux nécessaires à l\'aménagement et à l\'entretien d\'un jardin.\r\nUne jardinerie comporte bien souvent un rayon animalerie, avec ou sans vivant.\r\nExemples d\'enseignes nationales de jardinerie : Jardiland, Truffaut, Botanic, Gamm vert, VillaVerde, Baobab, Delbard.', 'b9722435636z-1-20200201171126-000-gvife830t-2-0-61bb6f70829fe786362844.jpg', 1, 'Jardinerie'),
(607, 2397, 'Ipsum dolor', '2021-12-16 16:56:34', '2021-12-16 16:56:34', 'Curabitur rutrum purus sed nulla efficitur bibendum. Ut lacinia lacus ac nunc tempus, a dictum nisl tincidunt. Aliquam ut augue lorem. Phasellus vel enim id sapien dignissim fringilla. Suspendisse lacinia efficitur diam ac tincidunt. Maecenas gravida id augue non scelerisque. Curabitur volutpat scelerisque lectus, sed mollis elit.', 'pexels-sebastiaan-stam-1097456-61bb6fc2a44a4110754814.jpg', 1, 'Ipsum dolor'),
(608, 2349, 'Nouvelle version phpstorm', '2021-12-16 18:08:22', '2021-12-16 18:08:22', 'Maecenas quis pretium elit, et tincidunt erat. Phasellus vel ornare felis. Vivamus eu mi et massa vestibulum pulvinar nec et tellus. Aliquam ut efficitur leo. Praesent in magna tempus, pellentesque odio non, pharetra felis. Morbi turpis tellus, cursus volutpat accumsan vitae, porttitor sed metus.', 'blog-2x-1-61bb80966748e388605703.jpg', 1, 'nouvelle-version-phpstorm'),
(609, 2398, 'Magni ut ipsa', '2021-12-16 17:41:10', '2021-12-16 17:41:10', 'Phasellus luctus luctus accumsan. Nullam mi tortor, suscipit dapibus metus sed, feugiat imperdiet ex. Pellentesque accumsan sem vel ipsum lobortis ullamcorper. Proin nec pretium ipsum, eu sagittis lacus. Fusce vestibulum magna pretium, iaculis nisl id, aliquet purus. Pellentesque sit amet interdum sapien', 'pexels-andrea-piacquadio-733872-61bb7a36a12fd517197304.jpg', 1, 'Magni ut ipsa');

-- --------------------------------------------------------

--
-- Structure de la table `teacher`
--

DROP TABLE IF EXISTS `teacher`;
CREATE TABLE IF NOT EXISTS `teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `teacher`
--

INSERT INTO `teacher` (`id`, `first_name`, `last_name`, `email`, `image`) VALUES
(9, 'Thibault', 'Morel', 'Thibault.Morel@gmail.com', 'morel.jpg'),
(10, 'Capucine', 'Descamps', 'Capucine.Descamps@gmail.com', 'descamps.jpg'),
(11, 'Bernard', 'Leroux', 'Bernard.Leroux@gmail.com', 'leroux.jpg'),
(12, 'Nath', 'Labbe', 'Nath.Labbe@gmail.com', 'Labbe.jpg'),
(13, 'Océane', 'Rolland', 'Océane.Rolland@gmail.com', 'rolland.jpg'),
(14, 'Philippe', 'Leblanc', 'Philippe.Leblanc@gmail.com', 'leblanc.jpg'),
(15, 'Martin', 'Gonzalez', 'Martin.Gonzalez@gmail.com', 'gonzalez.jpg'),
(16, 'Marcel', 'Samson', 'Marcel.Samson@gmail.com', 'samson.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `last_log_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `is_disabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2399 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`, `first_name`, `last_name`, `image`, `created_at`, `updated_at`, `last_log_at`, `is_disabled`) VALUES
(2345, 'jules.neveu@gmail.com', '[\"ROLE_USER\"]', '$2y$13$dyrKAmKypGDQXO0jYzc7L.YHkVJgAaLvtcGxR05e2.qpKnfn3vDMy', 'Jules', 'Neveu', '011m.jpg', '2021-12-13 17:48:14', '2021-12-13 17:48:14', '2021-12-13 17:48:14', 0),
(2346, 'jean.delattre@gmail.com', '[\"ROLE_USER\"]', '$2y$13$OnMJ6563SikeO8isnue7w.nsaDmQvSg9uWV5pO9WXXJ6G4L2cVOkS', 'Jean', 'Delattre', '012m.jpg', '2021-12-13 17:48:14', '2021-12-13 17:48:14', '2021-12-13 17:48:14', 0),
(2347, 'valentine.guilbert@gmail.com', '[\"ROLE_USER\"]', '$2y$13$UdPNDJsmgkT/ee5Wjc0hR.L9CH3qLsroZ/SS8ji/e.pNuBTYgRcq.', 'Valentine', 'Guilbert', '013f.jpg', '2021-12-13 17:48:15', '2021-12-13 17:48:15', '2021-12-13 17:48:15', 0),
(2348, 'jacques.le-gall@gmail.com', '[\"ROLE_USER\"]', '$2y$13$VPaUZ.MqQJHoK77C1KwH8uWgl/tGd1YPa9arbmjuH.GCj1awVpope', 'Jacques', 'Le Gall', '014m.jpg', '2021-12-13 17:48:15', '2021-12-13 17:48:15', '2021-12-13 17:48:15', 0),
(2349, 'laurent.monnier@gmail.com', '[\"ROLE_USER\"]', '$2y$13$NWMHE7EGnlv0q8O/ITBSyuGX17fvnbZNE6ceOyJVSIt8o.oSsUb8O', 'Laurent', 'Monnier', '015m.jpg', '2021-12-13 17:48:16', '2021-12-13 17:48:16', '2021-12-13 17:48:16', 0),
(2350, 'antoine.grenier@gmail.com', '[\"ROLE_USER\"]', '$2y$13$Rfuqb5P2qiaBLUjPHNA11eACxA8zZHQciXE2AszImQdR3u95gbcby', 'Antoine', 'Grenier', '016m.jpg', '2021-12-13 17:48:16', '2021-12-13 17:48:16', '2021-12-13 17:48:16', 0),
(2351, 'philippe.evrard@gmail.com', '[\"ROLE_USER\"]', '$2y$13$6X8PKV65HRBIii8d8wpfmOpU7.HFd.WfQ8pwx4G1o6LYbLXpxInWe', 'Philippe', 'Evrard', '017m.jpg', '2021-12-13 17:48:17', '2021-12-13 17:48:17', '2021-12-13 17:48:17', 0),
(2352, 'maurice.carpentier@gmail.com', '[\"ROLE_USER\"]', '$2y$13$b3LdefkmvYGY1V2ddWwHCelbguVIPA0sHzVlM.X3BvXSC46s5nJgi', 'Maurice', 'Carpentier', '018m.jpg', '2021-12-13 17:48:17', '2021-12-13 17:48:17', '2021-12-13 17:48:17', 0),
(2353, 'patricia.perrin@gmail.com', '[\"ROLE_USER\"]', '$2y$13$ZaUYL597l7cRrmhAkuuQUe60ruozr4SuiXMS9O6UlKguLFhupCcA.', 'Patricia', 'Perrin', '019f.jpg', '2021-12-13 17:48:18', '2021-12-13 17:48:18', '2021-12-13 17:48:18', 0),
(2354, 'marine.morel@gmail.com', '[\"ROLE_USER\"]', '$2y$13$4utLCo4fG1KVv.a6JzQzveIzQjSvJtjtebT3VvxsXyXdecNhVfyz.', 'Marine', 'Morel', '020f.jpg', '2021-12-13 17:48:18', '2021-12-13 17:48:18', '2021-12-13 17:48:18', 0),
(2355, 'sebastien.levy@gmail.com', '[\"ROLE_USER\"]', '$2y$13$oxEFuIFEsII8exUL6OHfR.C6gw21AZ7SuXmpsVVvpON/0Ot87R49a', 'Sébastien', 'Levy', '021m.jpg', '2021-12-13 17:48:18', '2021-12-13 17:48:18', '2021-12-13 17:48:18', 0),
(2356, 'sylvie.aubert@gmail.com', '[\"ROLE_USER\"]', '$2y$13$UJjL9XH9d23wShaSV5SF2.eY1hW1BdNSPnOXxFtLuSGFc80MdtcDy', 'Sylvie', 'Auberte', '022f.jpg', '2021-12-13 17:48:19', '2021-12-13 17:48:19', '2021-12-13 17:48:19', 0),
(2357, 'aime.chevallier@gmail.com', '[\"ROLE_USER\"]', '$2y$13$iUx5Whxu.KsqGDIdKnPu3uaet49XvKajZJv12EiUEKyCTrTTjNm5G', 'Aimé', 'Chevallier', '023m.jpg', '2021-12-13 17:48:19', '2021-12-13 17:48:19', '2021-12-13 17:48:19', 0),
(2358, 'suzanne.joly@gmail.com', '[\"ROLE_USER\"]', '$2y$13$HLCO/58VGi6DX6pRlYHUqeqJ1S7/URyZYMYxfoFn5tyX7q4LoipSO', 'Suzanne', 'Joly', '024f.jpg', '2021-12-13 17:48:20', '2021-12-13 17:48:20', '2021-12-13 17:48:20', 0),
(2359, 'auguste.laroche@gmail.com', '[\"ROLE_USER\"]', '$2y$13$K8Q9Ra.y6qKkiN0d8wlkhOmYgvUkyXPUyO3LgnwHOhif7mDGF4kTO', 'Auguste', 'Laroche', '025m.jpg', '2021-12-13 17:48:20', '2021-12-13 17:48:20', '2021-12-13 17:48:20', 0),
(2360, 'victor.sanchez@gmail.com', '[\"ROLE_USER\"]', '$2y$13$6PNHRcWj080.lplbu1XfEeq7MWDpQwbctedrWTO0EghmeLIb0Bpfa', 'Victor', 'Sanchez', '026m.jpg', '2021-12-13 17:48:21', '2021-12-13 17:48:21', '2021-12-13 17:48:21', 0),
(2361, 'emmanuelle.levy@gmail.com', '[\"ROLE_USER\"]', '$2y$13$yK5oDI2ys/HQdDg8.pjDau03lOXTsJ75v8HkJn8thQmpfNoUrAEpW', 'Emmanuelle', 'Levy', '027f.jpg', '2021-12-13 17:48:21', '2021-12-13 17:48:21', '2021-12-13 17:48:21', 0),
(2362, 'frederic.regnier@gmail.com', '[\"ROLE_USER\"]', '$2y$13$8kptttnaDxhXtUGpYh/MheOUP6gKmoZlL0zVmSGODPq8rUa1d8fZ6', 'Frédéric', 'Regnier', '028m.jpg', '2021-12-13 17:48:22', '2021-12-13 17:48:22', '2021-12-13 17:48:22', 0),
(2363, 'patrick.imbert@gmail.com', '[\"ROLE_USER\"]', '$2y$13$RLPT7z1/FcJyX5LbFImZmu5Pq3Ahi6qWsGQH0Fbw9B3/Rfemc.zG2', 'Patrick', 'Imbert', '029m.jpg', '2021-12-13 17:48:22', '2021-12-13 17:48:22', '2021-12-13 17:48:22', 0),
(2364, 'monique.devaux@gmail.com', '[\"ROLE_USER\"]', '$2y$13$CRn0sc5Ps3TG/tP2TyBLm.wKlYCSv.uAECSQXHEgPb6xQJ3W3okrO', 'Monique', 'Devaux', '030f.jpg', '2021-12-13 17:48:23', '2021-12-13 17:48:23', '2021-12-13 17:48:23', 1),
(2365, 'agathe.salmon@gmail.com', '[\"ROLE_USER\"]', '$2y$13$O49577nUE4JdJjUAoZM9nOtueoamHQ2awDXU.SebHoBc6J4AwFwyu', 'Agathe', 'Salmon', '031f.jpg', '2021-12-13 17:48:23', '2021-12-13 17:48:23', '2021-12-13 17:48:23', 1),
(2366, 'louis.sauvage@gmail.com', '[\"ROLE_USER\"]', '$2y$13$DckZyCX0StyXjBImbWxnruTXcACkGJ1WAP5oyk5ZUGcxaj3UR6PZC', 'Louis', 'Sauvage', '032m.jpg', '2021-12-13 17:48:23', '2021-12-13 17:48:23', '2021-12-13 17:48:23', 0),
(2367, 'guy.barre@gmail.com', '[\"ROLE_USER\"]', '$2y$13$AH6OiDowNOU3ZHoWz0sbuuyrtjp6m5j6VnLlg9Kiy0tardx40kTYW', 'Guy', 'Barre', '033m.jpg', '2021-12-13 17:48:24', '2021-12-13 17:48:24', '2021-12-13 17:48:24', 0),
(2368, 'helene.benoit@gmail.com', '[\"ROLE_USER\"]', '$2y$13$KlM0myenEJYd58EzbPXBIu9uHVRmlITxX9bD7ExMdOxuC4C/b.RwO', 'Hélène', 'Benoit', '034f.jpg', '2021-12-13 17:48:24', '2021-12-13 17:48:24', '2021-12-13 17:48:24', 0),
(2369, 'alexandre.bonnet@gmail.com', '[\"ROLE_USER\"]', '$2y$13$K9sFsmOrYO41iPjpy0QkEen4Lv1xgrWbtNGB3o5wy09atCvzskBRu', 'Alexandre', 'Bonnet', '035m.jpg', '2021-12-13 17:48:25', '2021-12-13 17:48:25', '2021-12-13 17:48:25', 0),
(2370, 'antoinette.allard@gmail.com', '[\"ROLE_USER\"]', '$2y$13$V54eAIv36dQ0CvLUoC3ITuVXZypEZSXhPD5oc1xtMKsl1lqCM4zu.', 'Antoinette', 'Allard', '036f.jpg', '2021-12-13 17:48:25', '2021-12-13 17:48:25', '2021-12-13 17:48:25', 0),
(2371, 'marie.launay@gmail.com', '[\"ROLE_USER\"]', '$2y$13$WRYs3T5MPy2a8kjGnhP4Quj6XqzKecuXkjZA.XhbkMxvXRFDqDLDm', 'Marie', 'Launay', '037f.jpg', '2021-12-13 17:48:26', '2021-12-13 17:48:26', '2021-12-13 17:48:26', 0),
(2372, 'margaret.langlois@gmail.com', '[\"ROLE_USER\"]', '$2y$13$ISXHVmwvciAjSW7LCXtXgeD.1DKkrqJqkTlLFG3KALgXKuoPS.P6i', 'Margaret', 'Langlois', '038f.jpg', '2021-12-13 17:48:26', '2021-12-13 17:48:26', '2021-12-13 17:48:26', 0),
(2373, 'helene.mendes@gmail.com', '[\"ROLE_USER\"]', '$2y$13$4oo7/H6wHP4HHflFG4KEqePmyZX3IdonK3PBkmfxH2ymiwj2eQInm', 'Hélène', 'Mendes', '039f.jpg', '2021-12-13 17:48:27', '2021-12-13 17:48:27', '2021-12-13 17:48:27', 0),
(2374, 'audrey.pruvost@gmail.com', '[\"ROLE_USER\"]', '$2y$13$BemgYTH5zdfKZ1tM0XY5/eV/Lp86tVhraq5aO.OjECA82l2W0525e', 'Audrey', 'Pruvost', '040f.jpg', '2021-12-13 17:48:27', '2021-12-13 17:48:27', '2021-12-13 17:48:27', 0),
(2375, 'vincent.lacroix@gmail.com', '[\"ROLE_USER\"]', '$2y$13$C6.obW9r0rFI5GkN/HfdKeZqAEKUyd4vtdLza6vRb/aiz/uWwoRRe', 'Vincent', 'Lacroix', '041m.jpg', '2021-12-13 17:48:28', '2021-12-13 17:48:28', '2021-12-13 17:48:28', 0),
(2376, 'claude.bonnin@gmail.com', '[\"ROLE_USER\"]', '$2y$13$.IkHJX.JRsLJG/rl2NJxA.VaIt2kMyDs.htiqz318r0a8ksynKHMC', 'Claude', 'Bonnin', '042m.jpg', '2021-12-13 17:48:28', '2021-12-13 17:48:28', '2021-12-13 17:48:28', 0),
(2377, 'denise.breton@gmail.com', '[\"ROLE_USER\"]', '$2y$13$WgxZfahdnjpbTbpocf.D/eTnZf3YFvy1RnlC5OwBT//WxyIR04sGu', 'Denise', 'Breton', '043f.jpg', '2021-12-13 17:48:28', '2021-12-13 17:48:28', '2021-12-13 17:48:28', 0),
(2378, 'mathilde.andre@gmail.com', '[\"ROLE_USER\"]', '$2y$13$zMA/jeJhi3eNutvYL2sDae5xA/TrSFVgCKmYtKWmU0Q4BU.UX69ZK', 'Mathilde', 'Andre', '044f.jpg', '2021-12-13 17:48:29', '2021-12-13 17:48:29', '2021-12-13 17:48:29', 0),
(2379, 'claude.gaudin@gmail.com', '[\"ROLE_USER\"]', '$2y$13$133DOlEDUF7lIZA0tuiTQ.vnuGp5nfgBG9g9ai378PA4Cle98lJXO', 'Claude', 'Gaudin', '045m.jpg', '2021-12-13 17:48:29', '2021-12-13 17:48:29', '2021-12-13 17:48:29', 1),
(2380, 'thibault.ollivier@gmail.com', '[\"ROLE_USER\"]', '$2y$13$4fMIhRwbYosJDtKDUCYkVerI2baDqqc/lZtAtyxlDgQSylTs9zHtO', 'Thibault', 'Ollivier', '046m.jpg', '2021-12-13 17:48:30', '2021-12-13 17:48:30', '2021-12-13 17:48:30', 0),
(2381, 'leon.potier@gmail.com', '[\"ROLE_USER\"]', '$2y$13$Lk8MWoc6XWyAus.RK3KJDuLa2SJXAAWfJEOwD9pACblXHOmReKfDq', 'Léon', 'Potier', '047m.jpg', '2021-12-13 17:48:30', '2021-12-13 17:48:30', '2021-12-13 17:48:30', 0),
(2382, 'aurelie.david@gmail.com', '[\"ROLE_USER\"]', '$2y$13$OPj7t2fWq7kYCNHoOMNU1.awxdKHB5311PWYupF4RY9i7p0b0JRsC', 'Aurélie', 'David', '048f.jpg', '2021-12-13 17:48:31', '2021-12-13 17:48:31', '2021-12-13 17:48:31', 0),
(2383, 'margaux.chretien@gmail.com', '[\"ROLE_USER\"]', '$2y$13$Ct7uEOr2MUZusZgNGIC1dOQR6c3P1SQjQ8Qd1zGBuw/Kw4HqsWweO', 'Margaux', 'Chretien', '049f.jpg', '2021-12-13 17:48:31', '2021-12-13 17:48:31', '2021-12-13 17:48:31', 0),
(2384, 'gabrielle.leclerc@gmail.com', '[\"ROLE_USER\"]', '$2y$13$0mE4Cnqh9aXrNAyavySkAeOWLxM.A8/USU1E7CfKB2EUXn6UlhPR2', 'Gabrielle', 'Leclerc', '050f.jpg', '2021-12-13 17:48:32', '2021-12-13 17:48:32', '2021-12-13 17:48:32', 0),
(2385, 'henriette.lefebvre@gmail.com', '[\"ROLE_USER\"]', '$2y$13$YlI7EWwdHcyjT7JpKNlWBeMUqXfzVTyHBS409QG7jg30h/fkZWxRC', 'Henriette', 'Lefebvre', '051f.jpg', '2021-12-13 17:48:32', '2021-12-13 17:48:32', '2021-12-13 17:48:32', 0),
(2386, 'suzanne.chevallier@gmail.com', '[\"ROLE_ADMIN\"]', '$2y$13$OS9EjGtWGZHMZCX5am8FW.0rVz7dMF/MqF0Ve4eAsg4K8I4L79JQm', 'Suzanne', 'Chevallier', '052f.jpg', '2021-12-13 17:48:33', '2021-12-13 17:48:33', '2021-12-13 17:48:33', 0),
(2387, 'gilles.pineau@gmail.com', '[\"ROLE_USER\"]', '$2y$13$JQJmeLfGwFhhtgNmBYJ9ZukHs9AhS5TJiNk6XMW6UATVoUHqJsajK', 'Gilles', 'Pineau', '053m.jpg', '2021-12-13 17:48:33', '2021-12-13 17:48:33', '2021-12-13 17:48:33', 1),
(2388, 'olivie.albert@gmail.com', '[\"ROLE_USER\"]', '$2y$13$Csg6Fue7JVTfIAS1TSftyeR1qSI.O1EWllYG5AW5vNh38VU7q1KVS', 'Olivie', 'Albert', '054f.jpg', '2021-12-13 17:48:33', '2021-12-13 17:48:33', '2021-12-13 17:48:33', 0),
(2389, 'juliette.rey@gmail.com', '[\"ROLE_USER\"]', '$2y$13$xMKWLkZkoVNONHL6VSnLUOPPfrhOdoBlYhXzRjMQNdAHFwcwsAH0C', 'Juliette', 'Rey', '055f.jpg', '2021-12-13 17:48:34', '2021-12-13 17:48:34', '2021-12-13 17:48:34', 0),
(2390, 'valerie.guillon@gmail.com', '[\"ROLE_USER\"]', '$2y$13$WA5mDfxxonUkjW55hSMA6OKt5kCKIdmwuG85CTizZcINwprLKaM7i', 'Valérie', 'Guillon', '056f.jpg', '2021-12-13 17:48:34', '2021-12-13 17:48:34', '2021-12-13 17:48:34', 0),
(2391, 'chantal.lesage@gmail.com', '[\"ROLE_USER\"]', '$2y$13$ZafHoirB06uMoBC2yFrj8.W8R3PiNtJD7Yz3w.u39RrAN5Qtbo4O2', 'Chantal', 'Lesage', '057f.jpg', '2021-12-13 17:48:35', '2021-12-13 17:48:35', '2021-12-13 17:48:35', 0),
(2392, 'luc.dupuy@gmail.com', '[\"ROLE_USER\"]', '$2y$13$so/RwOvOn84uqkG4nelcWOUpE89BfQ9RUfXwpoJp2D15nXSBSYdka', 'Luc', 'Dupuy', '058m.jpg', '2021-12-13 17:48:35', '2021-12-13 17:48:35', '2021-12-13 17:48:35', 0),
(2393, 'jacqueline.guibert@gmail.com', '[\"ROLE_USER\"]', '$2y$13$2iS/NvBt0Juh.XdaDA9BC.OL9N9ZTpC2OdPF4q0YffI126mhuRWKS', 'Jacqueline', 'Guibert', '059f.jpg', '2021-12-13 17:48:36', '2021-12-13 17:48:36', '2021-12-13 17:48:36', 0),
(2394, 'anne.marin@gmail.com', '[\"ROLE_USER\"]', '$2y$13$PlOslohbASp/mbqSigQfV.ivU.QQTExK4bOi/ktZcPs/DiB0Pb4qe', 'Anne', 'Marin', '060f.jpg', '2021-12-13 17:48:36', '2021-12-13 17:48:36', '2021-12-13 17:48:36', 0),
(2395, 'john.doe@gmail.com', '[\"ROLE_ADMIN\"]', '$2y$13$SuGTMrmcRQgcjaRDNA5zBOfQJyom7/oIgXHjzdscPGCDFQaBF1UqO', 'John', 'Doe', '075m.jpg', '2021-12-13 17:48:37', '2021-12-13 17:48:37', '2021-12-13 17:48:37', 0),
(2396, 'pat.mar@gmail.com', '[\"ROLE_SUPER_ADMIN\"]', '$2y$13$Vg/G39qM4umdBR1RjLknze3qvNLUpbo52m.vFkrJ8WkqGbbXU2q6C', 'Pat', 'Mar', '071m-61b7a1873a669399335268.jpg', '2021-12-13 17:48:37', '2021-12-13 19:39:51', '2021-12-13 17:48:37', 0),
(2397, 'adrientrompette@gmail.com', '[\"ROLE_SUPER_ADMIN\"]', '$2y$13$Bq3s/6v01zxy54JHmt93rOE60aT7xLjceSA104/hWGXu76uUxtzOO', 'Adrien', 'Trompette', '33963873-1807079466002401-4750330109657874432-n-61bb7bcd4670a638073431.jpg', '2021-12-13 17:48:38', '2021-12-16 17:47:57', '2021-12-13 17:48:38', 0),
(2398, 'isabellegilmard@hotmail.com', '[\"ROLE_USER\"]', '$2y$13$94gTXTup4fuKOpkfLx8KZ.RWHO4lCsQ2K4G2g7N0adHCb05PuXFFy', 'isabelle', 'gilmard', '22-pixie-cut-with-long-bangs-bbpnf8ulp-8-61bb80032e8de790647316.jpg', '2021-12-16 17:34:31', '2021-12-16 18:05:55', '2021-12-16 17:34:31', 0);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FK_9474526C591CC992` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`),
  ADD CONSTRAINT `FK_9474526CF675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `course`
--
ALTER TABLE `course`
  ADD CONSTRAINT `FK_169E6FB912469DE2` FOREIGN KEY (`category_id`) REFERENCES `course_category` (`id`),
  ADD CONSTRAINT `FK_169E6FB941807E1D` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`id`),
  ADD CONSTRAINT `FK_169E6FB95FB14BA7` FOREIGN KEY (`level_id`) REFERENCES `course_level` (`id`);

--
-- Contraintes pour la table `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `FK_1DD39950F675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
